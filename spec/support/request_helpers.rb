module RequestHelpers
  def json
    @json ||= JSON.parse(response.body)
  end

  def clear_json
    @json = nil
  end

  def json_items_ids
    json.map { |item| item['id'] }
  end

  def serialized(object)
    JSON.parse(
      ActiveModelSerializers::SerializableResource.new(object).to_json
    )
  end

  def decoded_jwt_token_from_response(response)
    token = response.headers['Authorization'].split(' ').last
    # hmac_secret = ENV['DEVISE_JWT_SECRET_KEY']
    hmac_secret = '3bb4f756a8c873e22ce0c60c3dda05e4021bb34099204236ad621e4dd8f2685320dcbcfb2b588e496b0a227b1296fbeb5a167da5d71dc677001f1932a369c6be'
    JWT.decode token, hmac_secret, true
  end
end

Rails.application.routes.draw do
  devise_for :users,
             path: '',
             path_names: {
               sign_in: 'login',
               sign_out: 'logout',
               registration: 'signup'
             },
             controllers: {
               sessions: 'sessions',
               registrations: 'registrations'
             }

  devise_scope :user do
    post '/signup', to: 'registrations#create'
  end

  namespace :api do
    namespace :v1 do
      post '/endpoint_schemas', to: 'endpoint_schemas#create'

      resources :projects do
        member do
          get :documentation
        end
      end
      resources :documents
      resources :users, only: [] do
        collection do
          get :me
        end
      end
      namespace :users do
        resources :reset_password_tokens, only: [:create]
        resources :passwords, only: [:update], param: :token
      end
    end
  end
end

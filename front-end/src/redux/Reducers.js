const initialStates = {
    token: null
}

export default function reducer(state, action) {
    if (typeof state === 'undefined') {
        return initialStates
    }

    switch (action.type) {
        case 'LOG_IN':
            return Object.assign({}, state, {
                token: action.token
            });

        case 'LOG_OUT':
            localStorage.removeItem('token');
            return Object.assign({}, state, {
                token: null
            });

        default:
            return state
    }
}
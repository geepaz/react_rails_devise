import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Login } from './views/Login';
import { SignUp } from './views/SignUp';
import Home from './views/Home';

export const openRoutes = () => {
  return (
    <Router>
      <Route path="/" exact component={Login} />
      <Route path="/sign_up" exact component={SignUp} />
    </Router>
  );
}

export const secureRoutes = () => {
  return (
    <Router>
      <Route path="/" exact component={Home} />
    </Router>
  );
}
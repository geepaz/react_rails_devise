import React, { Component } from 'react';
import {
  Form,
} from 'antd';
import jwtDecode from 'jwt-decode';
import { connect } from 'react-redux';
import 'antd/dist/antd.css';
import logo from './logo.svg';
import './App.scss';

import { openRoutes as OpenRoutes, secureRoutes as SecureRoutes } from './Routes';
import { logIn } from './redux/Actions';

class App extends Component {

  componentWillMount() {
    if(localStorage.token){
      let token = localStorage.getItem('token');
      this.props.setLogIn(token);
    }
  }

  render() {
    if(this.props.token){
      return(
        <SecureRoutes />
      );
    } else {
      return(
        <OpenRoutes />
      );
    }

  }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(App);

const mapStateToProps = ({ token }) =>{
  return {
    token: token
  }
}

const mapDispatchToProps = (dispatchEvent) => {
  return {
    setLogIn: (token) => {
      dispatchEvent(logIn(token));
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WrappedNormalLoginForm);

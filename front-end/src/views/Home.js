import React from 'react';
import { Button } from 'antd';
import { connect } from 'react-redux';

// Local imports
import { logOut } from '../redux/Actions';

class Home extends React.PureComponent {
    secureCall = () => {
        let token = localStorage.token;

        fetch('http://localhost:5000/api/v1/documents', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + token,
            },
        }).then(res => {
            return res.json();
        }).then((res) => {
            alert(res.text)
        });
    }

    LogOut = () => {
        this.props.setLogOut();
    }

    render() {
        return (
            <div>
                <h4>Hello User</h4>
                <Button style={{ width: '100%' }} type="primary" onClick={this.secureCall.bind(this)} htmlType="submit" className="login-form-button">
                    Make Protected Api Call
                </Button>
                <Button style={{ width: '100%' }} type="primary" onClick={this.LogOut.bind(this)} htmlType="submit" className="login-form-button">
                    Log Out
                </Button>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatchEvent) => {
    return {
        setLogOut: () => {
            dispatchEvent(logOut());
        }
    }
}

export default connect(null, mapDispatchToProps)(Home);
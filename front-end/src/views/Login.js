import React from 'react';
import {
    Form, Icon, Input, Button, Checkbox,
} from 'antd';
import axios from 'axios';
import { connect } from 'react-redux';
import { logIn } from '../redux/Actions';

class NormalLoginForm extends React.Component {
    signUp = () => {
        this.props.history.push('/sign_up');
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                let user = {};
                let tempValues = {};
                tempValues["email"] = values.userName;
                tempValues["password"] = values.password;
                user["user"] = tempValues;
                console.log(this.props)
                debugger
                axios.post('http://localhost:5000/login', user)
                    .then(res => {
                        this.props.setlogIn(res.data.access_token);
                        // window.location = 'http://localhost:3000';
                    })
                    .catch(error => {
                        alert('error');
                        console.log(error)
                    });
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div className="App">
                <header className="App-header">
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <Form.Item>
                            {getFieldDecorator('userName', {
                                rules: [{ required: true, message: 'Please input your username!' }],
                            })(
                                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: 'Please input your Password!' }],
                            })(
                                <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('remember', {
                                valuePropName: 'checked',
                                initialValue: true,
                            })(
                                <Checkbox>Remember me</Checkbox>
                            )}
                            <a className="login-form-forgot" onClick={this.signUp}>Forgot password</a>
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="login-form-button">
                                Log in
                            </Button>
                        </Form.Item>
                        <Form.Item>
                            Or <a onClick={this.signUp}>register now!</a>
                        </Form.Item>
                    </Form>
                </header>
            </div>
        );
    }
}

export const Login = Form.create({ name: 'normal_login' })(NormalLoginForm);

const mapDispatchToProps = (dispatchEvent) => {
    return {
        setlogIn: (token) => {
            dispatchEvent(logIn(token));
        }
    }
}

export default connect(null, mapDispatchToProps)(Login);           

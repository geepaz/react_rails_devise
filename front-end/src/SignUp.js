import React, { Component } from 'react';
import {
  Form,
  Icon,
  Input,
  Button,
  Checkbox,
} from 'antd';
import jwtDecode from 'jwt-decode';
import axios from 'axios';
import 'antd/dist/antd.css';
import logo from './logo.svg';
import './App.scss';

class SignUp extends Component {
  state = {
    token: null
  }
  
  secureCall = () => {

    let token = this.state.token;

    fetch('http://localhost:5000/api/v1/documents', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': "Bearer " + token,
      },
    }).then(res => {
      return res.json();
    }).then((res)=>{
      alert(res.text)
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let user = {};
        // let tempValues = {};
        // tempValues["email"] = values.userName;
        // tempValues["password"] = values.password;
        // user["user"] = tempValues;
        user['email'] = values.email
        user['password'] = values.password
        user['password_confirmation'] = values.password_confirmation

        axios.post('localhost:5000/signup', user)
        .then( res => {this.setState({token: res.data.access_token})} )
        .catch( error =>  alert('error') );
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    if(this.state.token){
      return(
        <div className="App">
        <header className="App-header">
          <div className='container'>
            <div>
              <h4>Hello User</h4>
              <Button style={{width: '100%'}} type="primary" onClick={this.secureCall.bind(this)} htmlType="submit" className="login-form-button">
                    Make Protected Api Call 
              </Button>
            </div>
          </div>
        </header>
      </div>
      )
    } else {
      return (
      <div className="SignUp">
        <header className="App-header">
          <div className='container'>
            <Form onSubmit={this.handleSubmit} className="login-form">
              <Form.Item>
                {getFieldDecorator('email', {
                  rules: [{ required: true, message: 'Please input your username!' }],
                })(
                  <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="email" />
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator('password', {
                  rules: [{ required: true, message: 'Please input your Password!' }],
                })(
                  <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator('password_confirmation', {
                  rules: [{ required: true, message: 'Please input your Password!' }],
                })(
                  <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                )}
              </Form.Item>
              <Form.Item>
                <div>
                  <Button style={{width: '100%'}} type="primary" htmlType="submit" className="login-form-button">
                    Log in
                  </Button>
                </div>
                <div style={{color: 'white'}}>
                  Or <a href="">Login</a>
                </div>
              </Form.Item>
            </Form>
          </div>
        </header>
      </div>
    );
    }
  }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(SignUp);


export default WrappedNormalLoginForm;

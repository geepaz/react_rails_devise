module Api
  module V1
    class DocumentsController < ApplicationController
      before_action :authenticate_user!
      skip_authorization_check

      def index
        render json: {text: "Welcome To The App..."}
      end
    end
  end
end

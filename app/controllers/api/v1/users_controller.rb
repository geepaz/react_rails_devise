module Api
  module V1
    class UsersController < ApplicationController
      before_action :authenticate_user!
      skip_authorization_check
      def me
        authorize! :read, current_user
        render json: current_user
      end
    end
  end
end

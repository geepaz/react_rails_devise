class RegistrationsController < Devise::RegistrationsController
  respond_to :json

  def create
  	sign_up_params = {"email"=>params["email"], "password"=>params["password"], "password_confirmation"=>params["password_confirmation"]}
    user = CreateUser.new(sign_up_params).call
    render_resource(user)
  end
end

class Document < ApplicationRecord

  validates :name, :project, presence: true
end
